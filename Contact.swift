//
//  Contact.swift
//  MateuszFraczek-BabylonHealth
//
//  Created by Mass on 03.12.2015.
//  Copyright © 2015 Mass. All rights reserved.
//

import Foundation
import RealmSwift

class Contact: Object {
    
    dynamic var firstName = ""
    dynamic var surname = ""
    dynamic var address = ""
    dynamic var id = 0
    dynamic var phoneNumber = ""
    dynamic var email = ""
    dynamic var createdAt = ""
    dynamic var updatedAt = ""
    
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
