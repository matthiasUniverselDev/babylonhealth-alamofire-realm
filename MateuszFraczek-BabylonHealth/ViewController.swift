//
//  ViewController.swift
//  MateuszFraczek-BabylonHealth
//
//  Created by Mass on 03.12.2015.
//  Copyright © 2015 Mass. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

let kContatsURL = "http://fast-gorge.herokuapp.com/contacts"
let kAvatarURL = "http://api.adorable.io/avatar/40/"

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!    
    var mainArray : Results<Contact>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        getUserData()
        readTasksAndUpdateUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: TableView Stack
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ContactsTableViewCell

        let main = mainArray[indexPath.row]
        cell.firstNameLabel.text = main.firstName
        cell.surnameLabel.text = main.surname
        
        Alamofire.request(.GET, kAvatarURL + main.email)
            .response { response in

                if let image = response.2 {
                    cell.avatarImage.image = UIImage(data: image)
                }
        }
        
        cell.accessoryType = .DisclosureIndicator
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let main = mainArray {
            return main.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("detailsSegue", sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    // MARK: PrepareForSegue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detailsSegue" {
            let detailsVC: DetailsViewController = segue.destinationViewController as! DetailsViewController
            let indexPath = tableView.indexPathForSelectedRow
            
            detailsVC.thisTask = mainArray[indexPath!.row]
        }
    }
    
    // MARK: Request
    func getUserData() {
        
        let url: NSURL = NSURL(string: kContatsURL)!
        Alamofire.request(.GET, url, parameters: nil, encoding: ParameterEncoding.URL).responseJSON { (_, _, result) in
            switch result {
            case .Success(let data):
                let json = JSON(data)

                for (_, newJson) in json {

                    let updatedAt = newJson["updatedAt"].stringValue
                    let address = newJson["address"].stringValue
                    let firstName = newJson["first_name"].stringValue
                    let id = newJson["id"].intValue
                    let phoneNumber = newJson["phone_number"].stringValue
                    let surname = newJson["surname"].stringValue
                    let email = newJson["email"].stringValue
                    let createdAt = newJson["createdAt"].stringValue
                    
                    let contact = Contact()
                    contact.updatedAt = updatedAt
                    contact.address = address
                    contact.firstName = firstName
                    contact.id = id
                    contact.phoneNumber = phoneNumber
                    contact.surname = surname
                    contact.email = email
                    contact.createdAt = createdAt
                    
                    let aPredicate = NSPredicate(format: "id = %i", id)
                    let object = uiRealm.objects(Contact).filter(aPredicate)
                    
                    if object.count != 0 {
                        for obj in object {
                            if obj.id == contact.id {
                                print("dont save")
                            } else {
                                print("save")
                                do {
                                    try uiRealm.write({ () -> Void in
                                        uiRealm.add(contact)
                                    })
                                } catch {
                                    print("error")
                                }
                            }
                        }
                    } else {
                        do {
                            try uiRealm.write({ () -> Void in
                                uiRealm.add(contact)
                                self.readTasksAndUpdateUI()
                            })
                        } catch {
                            print("error")
                        }
                    }
                }
            case .Failure(_, let error):
                print("Request failed with error: \(error)")
            }
        }
    }

    func readTasksAndUpdateUI(){
        mainArray = uiRealm.objects(Contact)
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.tableView.reloadData()
        }
    }
}

