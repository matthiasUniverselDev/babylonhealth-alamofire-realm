//
//  DetailsViewController.swift
//  MateuszFraczek-BabylonHealth
//
//  Created by Mass on 03.12.2015.
//  Copyright © 2015 Mass. All rights reserved.
//

import UIKit
import Alamofire

class DetailsViewController: UIViewController {
    
    var thisTask: Contact!
    
    @IBOutlet var firstNameLabel: UILabel!
    @IBOutlet var surnameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var createdAtLabel: UILabel!
    @IBOutlet var updatedAtLabel: UILabel!
    @IBOutlet var avatarImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firstNameLabel.text = thisTask.firstName
        surnameLabel.text = thisTask.surname
        addressLabel.text = thisTask.address
        emailLabel.text = thisTask.email
        phoneLabel.text = thisTask.phoneNumber
        createdAtLabel.text = thisTask.createdAt
        updatedAtLabel.text = thisTask.updatedAt
        
        getBigImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getBigImage() {
        
        let bigImageURL = kAvatarURL.stringByReplacingOccurrencesOfString("40", withString: "80")
        Alamofire.request(.GET, bigImageURL + thisTask.email)
            .response { response in
                
                if let image = response.2 {
                    self.avatarImage.image = UIImage(data: image)
                }
        }
    }
}
